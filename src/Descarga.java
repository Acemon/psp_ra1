import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by DAM on 23/11/2016.
 */
public class Descarga extends Observable implements ActionListener {
    JLabel lDescarga;
    JProgressBar pbDescarga;
    JButton btCancelar;
    JButton btEliminar;
    JPanel panel1;
    JLabel lVelocidad;
    JLabel lDatos;
    String ruta;
    String url;
    String cantidad;
    Download download;
    String nombreArchivo;
    private Observer observer;

    public Descarga(String ruta, String text) {
        this.url=text;
        nombreArchivo = Util.nomArchivo(this.url);
        this.ruta = ruta+File.separator+nombreArchivo;
        lDescarga.setText(nombreArchivo);
        btCancelar.addActionListener(this);
        btEliminar.addActionListener(this);
        descarga();
    }


    private void descarga() {
        download = new Download(url, this.ruta);
        download.addPropertyChangeListener(new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent event) {
                if (event.getPropertyName().equals("Tamaño")) {
                    cantidad=String.valueOf(event.getNewValue());
                }
                if (event.getPropertyName().equals("Velocidad")) {
                    lVelocidad.setText(String.valueOf(event.getNewValue())+" KB/s");
                }
                if (event.getPropertyName().equals("Descarga")) {
                    lDatos.setText(event.getNewValue()+"MB / "+cantidad+" MB");
                }
                if (event.getPropertyName().equals("Progreso")) {
                    int cantidad= (int) event.getNewValue();
                    pbDescarga.setValue(cantidad);
                    if (cantidad==100){
                        btCancelar.doClick();
                    }
                }
            }
        });
        download.execute();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("Cancelar")){
            if (pbDescarga.getValue()==100){
                observer.update(this, "Fin");
                lVelocidad.setText("Finalizado");
                btCancelar.setEnabled(false);
            }else{
                download.parar = true;
                lVelocidad.setText("Cancelado");
                btCancelar.setText("Reiniciar");
                btCancelar.setActionCommand("Reiniciar");
            }

        }else if (e.getActionCommand().equals("Reiniciar")){
            descarga();
            btCancelar.setText("Cancelar");
            btCancelar.setActionCommand("Cancelar");

        }else if (e.getActionCommand().equals("Eliminar")){
            download.parar = true;
            observer.update(this, "Cancelar");
        }
    }

    public void addObserver(Observer observer) {
        this.observer =observer;
    }
}
