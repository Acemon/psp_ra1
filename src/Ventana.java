import javax.swing.*;
import java.awt.*;

/**
 * Created by DAM on 23/11/2016.
 */
public class Ventana {
    JPanel panel1;
    JButton btDescargar;
    JTextField tfURL;
    JPanel panelDescargas;
    JButton btCambiarRuta;
    JSlider slider1;
    JLabel lDescargas;
    JButton btLeer;
    JTextArea taComentarios;
    JTextArea taEspera;
    JFrame frame;

    public Ventana() {
        frame = new JFrame("Ventana");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(600,600);
        frame.setVisible(true);
        panelDescargas.setLayout(new BoxLayout(panelDescargas,BoxLayout.Y_AXIS));
    }
}
