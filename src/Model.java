import java.io.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

/**
 * Created by Fernando on 02/12/2016.
 */
public class Model {
    private HashMap<String, Descarga>descarga=new HashMap<>();
    private ArrayList<String> espera = new ArrayList<>();

    public void anadirPanel(Descarga des, String s) {
        descarga.put(s, des);
    }

    public Collection<Descarga> refescarPanel() {
        return descarga.values();
    }

    public void eliminarPanel(String nombre) {
        descarga.remove(nombre);
    }

    public boolean comprobar(String text) {
        if (descarga.get(text)==null){
            return false;
        }
        return true;
    }

    public void anadirDescarga(String text) {
        espera.add(text);
    }

    public boolean isEspera() {
        return espera.isEmpty();
    }

    public String obtenerEspera() {
        return espera.get(espera.size()-1);
    }

    public void eliminarEspera() {
        espera.remove(espera.size()-1);
    }

    public ArrayList<String> refrescarTa() {
        return espera;
    }

    public void escribirArchivo() throws IOException {
        FileWriter fw = new FileWriter("fichero.txt");
        PrintWriter pw = new PrintWriter("fichero.txt");
        pw.write("https://cdn.kernel.org/pub/linux/kernel/v4.x/testing/linux-4.9-rc7.tar.xz\n");
        pw.write("https://cdn.kernel.org/pub/linux/kernel/v4.x/linux-4.8.12.tar.xz\n");
        pw.write("https://cdn.kernel.org/pub/linux/kernel/v4.x/linux-4.4.36.tar.xz\n");
        pw.write("https://cdn.kernel.org/pub/linux/kernel/v4.x/linux-4.1.36.tar.xz\n");
        pw.write("https://cdn.kernel.org/pub/linux/kernel/v3.x/linux-3.18.45.tar.xz\n");
        pw.write("https://cdn.kernel.org/pub/linux/kernel/v3.x/linux-3.16.39.tar.xz\n");
        pw.close();
        if (fw!=null)
            fw.close();
    }

    public ArrayList<String> leerArchivo()  {
        FileReader fr = null;
        ArrayList<String> lineas= new ArrayList<>();
        //Necesito asegurarme de que el archivo se cierra, falle o no la lectura del archivo
        try {
            fr = new FileReader("fichero.txt");
            BufferedReader br = new BufferedReader(fr);
            String linea;
            while((linea=br.readLine())!=null){
                lineas.add(linea);
            }
            fr.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            return lineas;
        }

    }

    public void guardarRegistro(String s) throws IOException {
        FileWriter fichero = new FileWriter("archivo.txt", true);
        PrintWriter escritor = new PrintWriter(fichero);
        escritor.println(s);
        escritor.close();
        fichero.close();
    }

    public String cargarRegistro() throws IOException {
        BufferedReader buffer = new BufferedReader(new FileReader(new File("archivo.txt")));
        String texto="";
        String linea = null;
        while ((linea = buffer.readLine()) != null)
            texto=texto+linea+"\n";
        buffer.close();
        return texto;
    }
}
