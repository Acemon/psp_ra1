import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by DAM on 23/11/2016.
 */
public class Controller implements ActionListener, Observer, ChangeListener {
    Ventana view;
    Model model;
    String ruta;
    int limite;
    int cont;
    Logger logger = LogManager.getLogger(this);

    public Controller(Ventana view, Model model) {
        this.ruta=Constantes.RUTA_GUARDADO;
        this.view=view;
        this.model=model;
        try {
            model.escribirArchivo();
        } catch (IOException e) {
            Util.mensajeError("Error al crear el archivo");
            e.printStackTrace();
        }
        this.limite =view.slider1.getValue();
        cont=1;
        view.btDescargar.addActionListener(this);
        view.btCambiarRuta.addActionListener(this);
        view.btLeer.addActionListener(this);
        view.slider1.addChangeListener(this);
        try {
            String lineas=model.cargarRegistro();
            view.taComentarios.setText(lineas);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        switch (actionEvent.getActionCommand()){
            case "Descargar":
                if (model.comprobar(Util.nomArchivo(view.tfURL.getText()))){
                    Util.mensajeAyuda("Esta descarga ya se esta realizando");
                    break;
                }
                limitador();

                break;
            case "CambiarRuta":
                JFileChooser jfc = new JFileChooser();
                int opcion = jfc.showSaveDialog(jfc);
                if (opcion==0)
                    ruta = jfc.getCurrentDirectory().toString();
                break;

            case "Leer":
                for (String s : model.leerArchivo()){
                    model.anadirDescarga(s);
                    if (cont<=limite){
                        nuevaDescarga(model.obtenerEspera());
                        model.eliminarEspera();
                    }
                }
                break;
        }
        refrescarpanel();
    }

    @Override
    public void update(Observable o, Object arg) {
        Descarga bajada = (Descarga) o;
        String nombre = bajada.nombreArchivo;
        String traza=view.taComentarios.getText();
        if (arg.equals("Fin")){
            traza=traza+nombre+" finalizada."+"\n";
            try {
                model.guardarRegistro(nombre+" finalizada."+"\n");
            } catch (IOException e) {
                e.printStackTrace();
            }
//            logger.trace();

        }else if (arg.equals("Cancelar")){
            traza=traza+nombre+" cancelada."+"\n";
            try {
                model.guardarRegistro(nombre+" cancelada."+"\n");
            } catch (IOException e) {
                e.printStackTrace();
            }
//            logger.trace();
        }
        view.taComentarios.setText(traza);
        model.eliminarPanel(nombre);
        cont--;
        if (!model.isEspera()){
            nuevaDescarga(model.obtenerEspera());
            model.eliminarEspera();
        }

        refrescarpanel();
    }

    private void nuevaDescarga(String s) {
        Descarga descarga = new Descarga(ruta, s);
        model.anadirPanel(descarga, Util.nomArchivo(s));
        descarga.addObserver(this);
        view.panelDescargas.updateUI();
        cont++;
    }

    private void limitador() {
        if (cont>limite){
            Util.mensajeAyuda("Has alcanzado el limite de descargas, añadida a la espera");
            model.anadirDescarga(view.tfURL.getText());
        }else{
            nuevaDescarga();
        }
    }

    private void nuevaDescarga() {
        Descarga descarga = new Descarga(ruta, view.tfURL.getText());
        model.anadirPanel(descarga, Util.nomArchivo(view.tfURL.getText()));
        descarga.addObserver(this);
        view.panelDescargas.revalidate();
        view.panelDescargas.updateUI();
        cont++;
    }

    private void refrescarpanel() {
        view.panelDescargas.removeAll();
        for (Descarga descarga: model.refescarPanel()){
            view.panelDescargas.add(descarga.panel1);
        }
        view.panelDescargas.updateUI();
        view.taEspera.setText("");
        for (String s : model.refrescarTa()){
            view.taEspera.setText(view.taEspera.getText()+Util.nomArchivo(s)+"\n");
        }
        view.taEspera.updateUI();
    }

    @Override
    public void stateChanged(ChangeEvent e) {
        view.lDescargas.setText
                ("Descargas simultaneas "+view.slider1.getValue());
        limite=view.slider1.getValue();
    }
}
