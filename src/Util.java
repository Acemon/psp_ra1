import javax.swing.*;
import java.text.DecimalFormat;

/**
 * Created by Fernando on 02/12/2016.
 */
public class Util {
    public static String nomArchivo(String url){
        String[] troceado = url.split("/");
        return troceado[troceado.length - 1];
    }

    public static void mensajeAyuda(String a){
        JOptionPane.showMessageDialog
                (null,
                        a,
                        "Error",
                        JOptionPane.INFORMATION_MESSAGE);
    }

    public static void mensajeError(String a) {
        JOptionPane.showMessageDialog
                (null,
                        a,
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
    }

    public static String redondeo(double value){
        DecimalFormat df = new DecimalFormat("####0.00");
        return df.format(value);
    }
}
