import com.sun.org.apache.xpath.internal.SourceTree;

import javax.swing.*;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by DAM on 28/11/2016.
 */
public class Download extends SwingWorker<Void, String> {
    private String url;
    private String ruta;
    boolean parar;

    public Download(String url, String ruta) {
        this.url = url;
        this.ruta = ruta;
    }

    @Override
    protected Void doInBackground() throws MalformedURLException, FileNotFoundException, IOException {
        URL direccion = new URL(url);
        URLConnection conexion = direccion.openConnection();
        double tamanoFich = conexion.getContentLength();
        String cantidad = Util.redondeo(tamanoFich/8388608);
        firePropertyChange("Tamaño", 0, cantidad);

        InputStream in = direccion.openStream();
        FileOutputStream out = new FileOutputStream(ruta);

        byte[] bytes = new byte[2048];
        int longitud = 0;
        double progresoDescarga = 0;
        long tiempoDescarga= System.currentTimeMillis();
        double longitudacumulada=0;

        while ((longitud = in.read(bytes)) != -1) {
            if ((System.currentTimeMillis()-tiempoDescarga) >1000){
                double tiempopasado = (System.currentTimeMillis()-tiempoDescarga);
                double velocidad = ((longitudacumulada/8192)/(tiempopasado/1000));
                String speed = Util.redondeo(velocidad);
                firePropertyChange("Velocidad", 0, speed);
                tiempoDescarga=System.currentTimeMillis();
                longitudacumulada=0;
            }
            out.write(bytes, 0, longitud);
            longitudacumulada += longitud;
            progresoDescarga += longitud;
            String descarga = Util.redondeo(progresoDescarga/8388608);
            firePropertyChange("Descarga", 0, descarga);
            int progreso = (int) ((progresoDescarga*100)/tamanoFich);
            firePropertyChange("Progreso", 0, progreso);
            if (parar)
                return null;
        }
        in.close();
        out.close();

        firePropertyChange("Progreso", 0, 100);
        return null;
    }
}
