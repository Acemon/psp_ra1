/**
 * Created by DAM on 23/11/2016.
 */
public class App {
    public static void main(String[] args){
        Ventana view = new Ventana();
        Model model = new Model();
        Controller controller = new Controller(view, model);
    }
}
